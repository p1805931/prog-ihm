package localisation;
import javax.swing.*;

import java.awt.event.*;

public class ZoomOutAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuLocalisation fe;
	
	public ZoomOutAction(String txt,MenuLocalisation fe) {
		super(txt);
		this.fe=fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		fe.zoomOut();
	}
	
}
