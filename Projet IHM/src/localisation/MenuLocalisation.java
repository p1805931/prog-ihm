package localisation;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.ImageIcon;
import accueil.LienToAction;


public class MenuLocalisation extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage imgLocalisation;
	private BufferedImage imgItineraire;
	private ImageIcon iconLocalisation;
	private ImageIcon iconItineraire;
	private JLabel image;
	
	public MenuLocalisation() throws IOException {
		
		super();
		imgLocalisation = ImageIO.read(new File("assets/position.png"));
		imgItineraire = ImageIO.read(new File("assets/itineraire.png"));
		iconLocalisation = new ImageIcon(imgLocalisation);
		iconItineraire = new ImageIcon(imgItineraire);
		
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu sur la s�curit� du v�hicule");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(2,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Localisation de votre v�hicule");
        contentPaneUp.add(title,BorderLayout.NORTH);
        
        image = new JLabel(iconLocalisation);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(image);
        ScrollSetter scrollSetter = new ScrollSetter(scrollPane.getHorizontalScrollBar(),100);
        scrollSetter.run();
        //System.out.println(scrollPane.getHorizontalScrollBar().getModel().getValue());
        scrollSetter = new ScrollSetter(scrollPane.getHorizontalScrollBar(),550);
        scrollSetter.run();
        //System.out.println(scrollPane.getHorizontalScrollBar().getModel().getValue());
        scrollSetter = new ScrollSetter(scrollPane.getVerticalScrollBar(),200);
        scrollSetter.run();
        //System.out.println(scrollPane.getVerticalScrollBar().getModel().getValue());
        contentPaneUp.add(scrollPane);

        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());
        
        JPanel contentPaneBottom1 = new JPanel();
        contentPaneBottom1.setLayout(new GridLayout(3,1));
        
        JPanel plus_moins = new JPanel();
        plus_moins.setLayout(new FlowLayout());
        JButton plus = new JButton(new ZoomInAction("+",this));
        JButton moins = new JButton(new ZoomOutAction("-",this));
        plus_moins.add(plus);
        plus_moins.add(moins);
        
        contentPaneBottom1.add(plus_moins);
        JButton centrage = new JButton(/*new LocalisationAction(*/"Centrage sur le v�hicule"/*,this)*/);
        contentPaneBottom1.add(centrage);
        centrage.addActionListener(event ->{try {
			new MenuLocalisation().setVisible(true);
			this.setVisible(false);
		} catch (IOException e) {
			e.printStackTrace();
		}});

        JButton itineraire = new JButton(new ItineraireAction("Itin�raire jusqu'au v�hicule",this));
        contentPaneBottom1.add(itineraire);
        
        JButton retour = new JButton(new LienToAction("Retour",5));
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        
        contentPaneBottom.add(contentPaneBottom1,BorderLayout.CENTER);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
	
	public void setItineraire() {
		image.setIcon(iconItineraire);
	}
	public void setLocalisation() {
		image.setIcon(iconLocalisation);
	}
	
	public void zoomIn() {
		int w=image.getWidth();
		int h=image.getHeight();
		if(image.getIcon()==iconItineraire) {
			iconItineraire = new ImageIcon(ZoomImage(w+50,h+50,imgItineraire));
			image.setIcon(iconItineraire);
		}
		else {
			iconLocalisation = new ImageIcon(ZoomImage(w+50,h+50,imgLocalisation));
			image.setIcon(iconLocalisation);
		}
		
	}
	
	public void zoomOut() {
		int w=image.getWidth();
		int h=image.getHeight();
		if(image.getIcon()==iconItineraire) {
			iconItineraire = new ImageIcon(ZoomImage(w-50,h-50,imgItineraire));
			image.setIcon(iconItineraire);
		}
		else {
			iconLocalisation = new ImageIcon(ZoomImage(w-50,h-50,imgLocalisation));
			image.setIcon(iconLocalisation);
		}
		
	}
	
	private BufferedImage ZoomImage(int w,int h,Image img) {
		BufferedImage buf = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
		Graphics2D grf=buf.createGraphics();
		grf.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		grf.drawImage(img, 0,0,w,h,null);
		grf.dispose();
		return buf;
	}
	
	class ScrollSetter extends Thread{
	      JScrollBar scroll;
	      int value;
	       
	      public ScrollSetter(JScrollBar scroll, int value){
	         this.scroll = scroll;
	         this.value = value;
	      }
	       
	      public void run(){
	         try{
	            Thread.sleep(100);
	            scroll.getModel().setValue(value);
	         } catch (InterruptedException e){
	            e.printStackTrace();
	         }
	      }
	   }
}
