package localisation;
import javax.swing.*;
import java.awt.event.*;

public class LocalisationAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuLocalisation fe;
	
	public LocalisationAction(String txt,MenuLocalisation fe) {
		super(txt);
		this.fe = fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		fe.setLocalisation();
	}
}
