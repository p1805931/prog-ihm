package confort;

import java.awt.*;
import java.io.IOException;
import javax.swing.*;
import accueil.LienToAction;
import application.MenuPrincipal;

public class MenuConfort extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean degriver;
	private boolean refrigerer;
	
	public MenuConfort() throws IOException {
		
		super();
		degriver=false;
		refrigerer=false;
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu sur le confort du v�hicule");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Gestion du confort de votre v�hicule");
        contentPaneUp.add(title);
        
        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JButton controleclim = new JButton(new LienToAction("Controle de la climatisation",13));
        JButton degivrage = new JButton("D�givrage");
        degivrage.addActionListener(event -> {
            if (degriver == true) {
            	JOptionPane.showMessageDialog(this, "D�givrage desactiv�","Param�trage d�givrage",JOptionPane.INFORMATION_MESSAGE);
            	degriver =false;
            }//true = verrouille et false = deverrouille
            else {
            	degriver = true;
            JOptionPane.showMessageDialog(this, "D�givrage activ�","Param�trage d�givrage",JOptionPane.INFORMATION_MESSAGE);
            }});
        JButton controlesiege = new JButton(new LienToAction("Contr�le si�ge chauffants",14));
        JButton boiteagant = new JButton("R�frig�ration de la bo�te � gant");
        boiteagant.addActionListener(event -> {
            if (refrigerer == true) {
            	JOptionPane.showMessageDialog(this, "R�frig�ration desactiv�","Param�trage r�frig�ration",JOptionPane.INFORMATION_MESSAGE);
            }//true = verrouille et false = deverrouille
            else {
            	refrigerer = true;
            JOptionPane.showMessageDialog(this, "R�frig�ration activ�","Param�trage r�frig�ration",JOptionPane.INFORMATION_MESSAGE);
            }});   
        
        contentPaneCenter.add(controleclim);
        contentPaneCenter.add(degivrage);
        contentPaneCenter.add(controlesiege);
        contentPaneCenter.add(boiteagant);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());

        JButton retour = new JButton(new LienToAction("Retour",5));
        retour.addActionListener(event ->{try {new MenuPrincipal();} catch (IOException e) {e.printStackTrace();}this.setVisible(false);});
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
}
