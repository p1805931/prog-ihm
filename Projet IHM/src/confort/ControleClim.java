package confort;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import accueil.LienToAction;

public class ControleClim extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ControleClim() throws IOException {
		
		super();
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu principal");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel climatisation = new JLabel("Climatisation");
        contentPaneUp.add(climatisation);
        
        JPanel contentPaneCenter = new JPanel();
        
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 30, 0);
        slider.setMinorTickSpacing(5);
        slider.setMajorTickSpacing(20);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setLabelTable(slider.createStandardLabels(10));
        contentPaneCenter.add(slider,BorderLayout.CENTER);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());

        JButton deconnexion = new JButton(new LienToAction("Retour",6));
        contentPaneBottom2.add(deconnexion,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);

        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
}
