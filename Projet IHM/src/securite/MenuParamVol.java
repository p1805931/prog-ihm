package securite;

import java.awt.*;
import java.io.IOException;
import javax.swing.*;
import accueil.LienToAction;

public class MenuParamVol extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean alerteValue;
	private JButton alerteButton;
	
	public MenuParamVol() throws IOException {
		super();
		alerteValue = true;
		alerteButton = new JButton(new ChangeAlerteAction("ON",this));
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu sur la s�curit� du v�hicule");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Param�trage d'alerte en cas de vol");
        contentPaneUp.add(title);
        
        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(3,1));
        
        JLabel alerte = new JLabel();
        alerte.setLayout(new FlowLayout());
        JLabel alerteTxt = new JLabel("Recevoir une notification :");
        
        alerte.add(alerteTxt);
        alerte.add(alerteButton);
        
        JButton appelPolice = new JButton(new AppelPoliceAction("Bouton appel police",this));
        JButton localisation = new JButton(new LienToAction("localisation du v�hicule",8));
        
        
        contentPaneCenter.add(alerte);
        contentPaneCenter.add(appelPolice);
        contentPaneCenter.add(localisation);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());
        
        JButton retour = new JButton(new LienToAction("Retour",9));
        retour.addActionListener(event ->{try {new MenuSecurite();}catch (IOException e) {e.printStackTrace();}this.setVisible(false);});
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
	
	public void changeAlerteValue() {
		alerteValue = !(alerteValue);
	}
	
	public boolean getAlerteValue() {
		return alerteValue;
	}
	
	public void setAlerteButton() {
		if(alerteValue) alerteButton.setText("ON");
		else alerteButton.setText("OFF");
	}
}
