package securite;

import javax.swing.*;
import java.awt.event.*;

public class AppelPoliceAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuParamVol fe;
	
	public AppelPoliceAction(String txt,MenuParamVol f) {
		super(txt);
		fe=f;
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(fe, "Appel de la police en cours");
	}
}
