package securite;

import java.awt.*;
import java.io.IOException;

import javax.swing.*;

import accueil.LienToAction;
import application.MenuPrincipal;

public class MenuSecurite extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean verrouillageValue;
	
	public MenuSecurite() throws IOException {
		super();
		verrouillageValue = true;
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Ecran de d�marrage");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Gestion de la s�curit� de votre v�hicule");
        contentPaneUp.add(title);
        
        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JButton verrouillage = new JButton("Verrouillage du v�hicule");
        verrouillage.addActionListener(event -> {
        if (verrouillageValue == true) {
        	JOptionPane.showMessageDialog(this, "Le v�hicule est d�j� verouill�","Erreur",JOptionPane.WARNING_MESSAGE,new ImageIcon("assets/cadenaslock.png"));
        }//true = verrouille et false = deverrouille
        else {
        verrouillageValue = true;
        JOptionPane.showMessageDialog(this, "Le v�hicule a �t� verouill�","Alerte",JOptionPane.WARNING_MESSAGE,new ImageIcon("assets/cadenaslock.png"));
        }});
        JButton deverrouillage = new JButton("D�verrouillage du v�hicule");
        deverrouillage.addActionListener(event -> {
            if (verrouillageValue == false) {
            	JOptionPane.showMessageDialog(this, "Le v�hicule est d�j� d�verouill�","Erreur",JOptionPane.WARNING_MESSAGE,new ImageIcon("assets/cadenasunlock.png"));
            }
            else {
            verrouillageValue = false;
            JOptionPane.showMessageDialog(this, "Le v�hicule a �t� d�verouill�","Alerte",JOptionPane.WARNING_MESSAGE,new ImageIcon("assets/cadenasunlock.png"));
            }});
        JButton paramVol = new JButton(new LienToAction("Param�trage d'alerte en cas de vol",10));
        paramVol.addActionListener(event ->{try {new MenuParamVol();} catch (IOException e) {e.printStackTrace();}this.setVisible(false);});
        JButton accesCam = new JButton(new LienToAction("Acc�s aux cam�ras du v�hicule",12));
        
        contentPaneCenter.add(verrouillage);
        contentPaneCenter.add(deverrouillage);
        contentPaneCenter.add(paramVol);
        contentPaneCenter.add(accesCam);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());
        
        JButton retour = new JButton(new LienToAction("Retour",5));
        retour.addActionListener(event ->{try {new MenuPrincipal();} catch (IOException e) {e.printStackTrace();}this.setVisible(false);});
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
}
