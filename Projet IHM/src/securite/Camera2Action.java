package securite;

import javax.swing.*;
import java.awt.event.*;

public class Camera2Action extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CameraDeSurveillance fe;
	
	public Camera2Action(String txt,CameraDeSurveillance fe) {
		super(txt);
		this.fe = fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		fe.setCamera2();
	}
}
