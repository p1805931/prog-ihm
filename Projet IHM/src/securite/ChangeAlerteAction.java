package securite;

import javax.swing.*;
import java.awt.event.*;

public class ChangeAlerteAction extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuParamVol fe;
	
	public ChangeAlerteAction(String txt, MenuParamVol f) {
		super(txt);
		fe=f;
	}
	
	public void actionPerformed(ActionEvent e) {
		fe.changeAlerteValue();
		fe.setAlerteButton();
	}
}
