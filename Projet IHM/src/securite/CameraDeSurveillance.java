package securite;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.ImageIcon;
import accueil.LienToAction;
import application.MenuPrincipal;

public class CameraDeSurveillance extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BufferedImage cam1;
	private BufferedImage cam2;
	private BufferedImage cam3;
	private ImageIcon img1;
	private ImageIcon img2;
	private ImageIcon img3;
	private JLabel image;
	
	public CameraDeSurveillance() throws IOException {
		super();
		cam1 = ImageIO.read(new File("assets/conducteur.png"));
		cam2 = ImageIO.read(new File("assets/voleur.png"));
		cam3 = ImageIO.read(new File("assets/siege.png"));
		img1 = new ImageIcon(cam1);
		img2 = new ImageIcon(cam2);
		img3 = new ImageIcon(cam3);
		build();
		insert();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu sur la s�curit� du v�hicule");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(2,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Acc�s aux cam�ras du v�hicule");
        contentPaneUp.add(title,BorderLayout.NORTH);
        
        image = new JLabel(img1);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(image);
        contentPaneUp.add(scrollPane);

        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());
        
        JPanel contentPaneBottom1 = new JPanel();
        contentPaneBottom1.setLayout(new GridLayout(3,1));
        
        
        
        JButton camera1 = new JButton(new Camera1Action("Cam1",this));
        contentPaneBottom1.add(camera1);
        JButton camera2 = new JButton(new Camera2Action("Cam2",this));
        contentPaneBottom1.add(camera2);
        JButton camera3 = new JButton(new Camera3Action("Cam3",this));
        contentPaneBottom1.add(camera3);
        
        JButton retour = new JButton(new LienToAction("Retour",9));
        retour.addActionListener(event ->{try {new MenuPrincipal();} catch (IOException e) {e.printStackTrace();}this.setVisible(false);});
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        
        
        contentPaneBottom.add(contentPaneBottom1,BorderLayout.CENTER);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
	
	public void setCamera1() {
		image.setIcon(img1);
	}
	public void setCamera2() {
		image.setIcon(img2);
	}
	public void setCamera3() {
		image.setIcon(img3);
	}
	
	
	
	
}
