package information;

import javax.swing.*;
import java.awt.event.*;

public class NiveauPneuAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuInformation fe;
	
	public NiveauPneuAction(String txt,MenuInformation fe) {
		super(txt);
		this.fe=fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(fe, fe.getNiveauPneus(),"Niveaux des pneus",JOptionPane.INFORMATION_MESSAGE);
	}
}
