package information;

import javax.swing.*;
import java.awt.event.*;

public class DiagnosticAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuInformation fe;
	
	public DiagnosticAction(String txt,MenuInformation fe) {
		super(txt);
		this.fe=fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(fe, "Autonomie :\n"+fe.getAutonomie()+"\n"+"Niveaux des pneus :\n"+fe.getNiveauPneus()+"\n"+"Charge du v�hicule :\n"+fe.getChargeVoiture(),"Diagnostic g�n�ral",JOptionPane.INFORMATION_MESSAGE);
	}
}
