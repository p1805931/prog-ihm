package information;

import javax.swing.*;
import java.awt.event.*;

public class ChargeAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuInformation fe;
	
	public ChargeAction(String txt,MenuInformation fe) {
		super(txt);
		this.fe=fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(fe, fe.getChargeVoiture(),"Charge du v�hicule",JOptionPane.INFORMATION_MESSAGE);
	}
}
