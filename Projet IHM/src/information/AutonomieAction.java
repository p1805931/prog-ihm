package information;

import javax.swing.*;
import java.awt.event.*;

public class AutonomieAction extends AbstractAction{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MenuInformation fe;
	
	public AutonomieAction(String txt,MenuInformation fe) {
		super(txt);
		this.fe=fe;
	}
	
	public void actionPerformed(ActionEvent e) {
		JOptionPane.showMessageDialog(fe, fe.getAutonomie(),"Autonomie",JOptionPane.INFORMATION_MESSAGE);
	}
}
