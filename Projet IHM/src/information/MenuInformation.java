package information;

import java.awt.*;
import java.io.IOException;
import javax.swing.*;
import accueil.LienToAction;

public class MenuInformation extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double autonElec;
	private double autonCarb;
	private int autonTotal;
	private double niv1;
	private double niv2;
	private double niv3;
	private double niv4;
	private int charge;
	
	
	
	public MenuInformation() throws IOException {
		super();
		
		autonElec=1.2;
		autonCarb=25.4;
		autonTotal = (int) ((autonElec+autonCarb)*10);
		niv1=2.5;
		niv2=2.3;
		niv3=2.6;
		niv4=2.5;
		charge = 300;
		
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu sur les informations du v�hicule");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        JLabel title = new JLabel("Acc�s aux informations de votre v�hicule");
        contentPaneUp.add(title,BorderLayout.CENTER);
        
        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JButton autonomie = new JButton(new AutonomieAction("Autonomie",this));
        JButton niveaupneus = new JButton(new NiveauPneuAction("Niveau des pneus",this));
        JButton diagnosticrapide = new JButton(new DiagnosticAction("Diagnostic rapide",this));
        JButton chargevoiture = new JButton(new ChargeAction("Charge de la voiture",this));
        
        
        contentPaneCenter.add(autonomie);
        contentPaneCenter.add(niveaupneus);
        contentPaneCenter.add(chargevoiture);
        contentPaneCenter.add(diagnosticrapide);
        
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());

        JButton retour = new JButton(new LienToAction("Retour",5));
        contentPaneBottom2.add(retour,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);
        
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
	
	public String getAutonomie() {
		return "Autonomie avec la batterie : "+autonElec+" KW/h\nAutonomie avec le carburant : "+autonCarb+" L\nAutonomie total en kilom�tre : "+autonTotal+" km";
	}
	
	public String getNiveauPneus() {
		return "Pneu Avant Gauche : "+niv1+" bar\nPneu Avant Droit : "+niv2+" bar\nPneu Arri�re Gauche : "+niv3+" bar\nPneu Arri�re Droit : "+niv4+" bar";
	}
	
	public String getChargeVoiture() {
		return "La voiture est charg� de "+charge+" kg";
	}
}
