package application;

import java.awt.*;
import java.io.IOException;
import javax.swing.*;
import accueil.LienToAction;

public class MenuPrincipal extends JFrame{
	
	/**
	 *  
	 */
	private static final long serialVersionUID = 1L;

	private JLabel title;
	
	public MenuPrincipal() throws IOException {
		super();
		build();
		insert();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Menu principal");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());
		
        title = new JLabel("Bienvenue ");
        contentPaneUp.add(title,BorderLayout.CENTER);
        
        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JButton securite = new JButton(new LienToAction("Sécurité",9));
        JButton confort = new JButton(new LienToAction("Confort",6));
        JButton information = new JButton(new LienToAction("Information",7));
        JButton localisation = new JButton(new LienToAction("Localisation",8));
        
        contentPaneCenter.add(securite);
        contentPaneCenter.add(confort);
        contentPaneCenter.add(information);
        contentPaneCenter.add(localisation);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());

        JButton deconnexion = new JButton("Deconnexion");
        deconnexion.addActionListener(event ->{
        	int reply =JOptionPane.showConfirmDialog(this, "Voulez-vous vraiment vous déconnecter ?","Déconnexion",JOptionPane.YES_NO_OPTION);
        	if(reply == JOptionPane.YES_OPTION) {
        		int i;
        		for(i=0;i<Launcher.getListePage().size();i++) {
        			Launcher.getListePage().get(i).setVisible(false);
        		}
        		Launcher.getListePage().get(0).setVisible(true);
        	}
        });
        JButton help = new JButton("?");
        help.addActionListener(event -> {
        	JOptionPane.showMessageDialog(this,"Nous sommes les développeurs, bon courage.","Help",JOptionPane.INFORMATION_MESSAGE);
        });
        contentPaneBottom2.add(help,BorderLayout.EAST);
        contentPaneBottom2.add(deconnexion,BorderLayout.WEST);
        contentPaneBottom.add(contentPaneBottom2,BorderLayout.SOUTH);

        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
	
	public void setBienvenue(String user) {
		title.setText("Bienvenue "+user); 
	}
}
