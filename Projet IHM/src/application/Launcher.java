package application;

import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import accueil.*;
import authentification.*;
import confort.*;
import information.*;
import localisation.*;
import securite.*;

public class Launcher {

	private static ArrayList<JFrame> listePage;
	private static ArrayList<String> mailList;
	private static ArrayList<String> mdpList;
	private static int ind;
	private static String user;
	
	public static void main(String[] args) {

		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
		try {
		listePage = new ArrayList<JFrame>();
		
        mailList = new ArrayList<String>();
        mdpList = new ArrayList<String>();
        mailList.add("admin");
        mailList.add("invite@gmail.com");
        mdpList.add("admin");
        mdpList.add("invite");
		
        
        MenuPrincipal menuPrin = new MenuPrincipal();
        Connexion connexion = new Connexion();
        Authentification authentification = new Authentification();

        ChangeMdp changeMdp = new ChangeMdp();
        mdpoublie mdpforget = new mdpoublie();
        Inscription inscription = new Inscription();
        MenuConfort menuConfort = new MenuConfort();
        MenuInformation menuInformation = new MenuInformation();
        MenuLocalisation menuLocalisation = new MenuLocalisation();
        MenuSecurite menuSecurite = new MenuSecurite();
        MenuParamVol menuParamVol = new MenuParamVol();
        ControleClim controleClim = new ControleClim();
        ControleSiegeChauffant controleSiegeChauffant = new ControleSiegeChauffant();
        CameraDeSurveillance cameraDeSurveillance = new CameraDeSurveillance();

        listePage.add(authentification); //0
        listePage.add(connexion); //1
        listePage.add(mdpforget); //2
        listePage.add(changeMdp); //3
        listePage.add(inscription); //4
        listePage.add(menuPrin); //5
        listePage.add(menuConfort); //6
        listePage.add(menuInformation); //7
        listePage.add(menuLocalisation); //8
        listePage.add(menuSecurite); //9
        listePage.add(menuParamVol); //10  
        EcranDeDemarrage ecranDem = new EcranDeDemarrage();
        listePage.add(ecranDem); //11
        listePage.add(cameraDeSurveillance);//12
        listePage.add(controleClim);//13
        listePage.add(controleSiegeChauffant);//14

		}
		catch(IOException e) {
			System.out.println("Pas d'image � cette destination");
		}
	}
	
	public static ArrayList<JFrame> getListePage(){
		return listePage;
	}
	
	public static ArrayList<String> getMailList(){
		return mailList;
	}
	
	public static ArrayList<String> getMdpList(){
		return mdpList;
	}

	public static int getInd() {
        return ind;
    }

    public static String getUser() {
        return getMailList().get(ind);
    }
	
	public static void setMdpList(String newMdp,int i) {
		mdpList.set(i, newMdp);
	}

	public static void ajoutMail(String newMail) {
		mailList.add(newMail);
	}
	
	public static void ajoutMdp(String newMdp) {
		mdpList.add(newMdp);
	}

    public static void setIndice(int i) {
        ind = i;
    }

    public static void setUser(String mail) {
        user = mail;
        System.out.println(user);
    }

}
