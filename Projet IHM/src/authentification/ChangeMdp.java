package authentification;
import javax.swing.*;

import accueil.LienToAction;
import application.Launcher;
import java.awt.*;

public class ChangeMdp extends JFrame{

        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		public ChangeMdp() {
            super();
            build();
            insert();
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //Display the window.
            this.setVisible(false);
        }

        private void build() {
            //set up the window.
            setTitle("Mot de passe oublie");
            setSize(360,748);

            Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
            this.setIconImage(icon);
            
            setLocationRelativeTo(null);
            setResizable(false);
        }

        private void insert() {
            //Global

            JPanel contentPane = new JPanel();
            contentPane.setLayout(new GridLayout(3,1));

            JPanel contentPaneUp = new JPanel();
            contentPaneUp.setLayout(new BorderLayout());

            JLabel title = new JLabel("Entrez votre nouveau mot de passe");
            contentPaneUp.add(title);

            JPanel contentPaneCenter = new JPanel();
            contentPaneCenter.setLayout(new GridLayout(2,1));

            JTextField mdp = new JTextField();
            JButton reintialise = new JButton("Reintialisez votre mot de passe");
            reintialise.addActionListener(event ->
            {
                boolean erreur = true;
                {
                    if (!(mdp.getText().isEmpty()))
                    {
                        erreur = false;
                    	Launcher.setMdpList(mdp.getText(),Launcher.getInd());
                    	int u;
                		for(u=0;u<Launcher.getListePage().size();u++) {
                			Launcher.getListePage().get(u).setVisible(false);
                		}
                		Launcher.getListePage().get(1).setVisible(true);
                    }
                }
                if(erreur)
                    JOptionPane.showMessageDialog(this,
                            "Veuillez mettre un mot de passe",
                            " ERREUR ",
                            JOptionPane.WARNING_MESSAGE);
            });

            contentPaneCenter.add(mdp);
            contentPaneCenter.add(reintialise);

            JPanel contentPaneBottom = new JPanel();
            contentPaneBottom.setLayout(new BorderLayout());

            JButton retour = new JButton(new LienToAction("Retour",1));

            contentPaneBottom.add(retour,BorderLayout.SOUTH);

            contentPane.add(contentPaneUp);
            contentPane.add(contentPaneCenter);
            contentPane.add(contentPaneBottom);

            contentPane.setOpaque(true);
            this.setContentPane(contentPane);
        }
    }
