package authentification;

import java.awt.*;
import javax.swing.*;
import accueil.LienToAction;
	
public class Authentification extends JFrame{

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Authentification() {
		
		super();
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(false);
	}
	
	private void build() {
		//set up the window.
        setTitle("Authenfication");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));
        
        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());

        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JButton connect = new JButton(new LienToAction("Se connecter",1));
        JButton inscrire = new JButton(new LienToAction("S'inscrire",4));

        contentPaneCenter.add(connect);
        contentPaneCenter.add(inscrire);
        
        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());
        JPanel contentPaneBottom2 = new JPanel();
        contentPaneBottom2.setLayout(new BorderLayout());
   
        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);
        
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
}
	


