package authentification;

import javax.swing.*;
import java.awt.*;
import accueil.LienToAction;
import application.Launcher;

public class Inscription extends JFrame{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Inscription() {
        super();
        build();
        insert();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Display the window.
        this.setVisible(false);
    }

    private void build() {
        //set up the window.
        setTitle("S'inscrire");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
    }

    private void insert() {
        //Global
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));

        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());

        JLabel title = new JLabel("S'inscrire");
        contentPaneUp.add(title);

        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));

        JLabel msgMail = new JLabel("Adresse mail  :");
        JTextField mail = new JTextField();
        JLabel msgMdp = new JLabel("Mot de passe  :");
        JPasswordField mdp = new JPasswordField();
        JLabel msgLink = new JLabel("Code vehicule :");
        JTextField link = new JTextField();
        JButton connexion = new JButton("S'inscrire");
        connexion.addActionListener(event ->
        {
        	String password = new String(mdp.getPassword());
            int i;
            boolean check = true;
            if(!(mail.getText().contains("@gmail.com")) || mail.getText().equals("@gmail.com"))
            JOptionPane.showMessageDialog(this,
                    "Veuillez mettre une adresse gmail.com",
                    " ERREUR ",
                    JOptionPane.WARNING_MESSAGE);

            else if(password.isEmpty())
                JOptionPane.showMessageDialog(this,
                        "Veuillez mettre un mot de passe",
                        " ERREUR ",
                        JOptionPane.WARNING_MESSAGE);

            else if(link.getText().isEmpty())
                JOptionPane.showMessageDialog(this,
                        "Veuillez associer la voiture et le compte via le code",
                        " ERREUR ",
                        JOptionPane.WARNING_MESSAGE);

            else
                {
                    for(i=0 ; i < Launcher.getMailList().size(); i++)
                    {
                        if (mail.getText().equals(Launcher.getMailList().get(i)))
                        {
                            JOptionPane.showMessageDialog(this,
                                    "Email deja existant",
                                    " ERREUR ",
                                    JOptionPane.WARNING_MESSAGE);
                            check = false;
                        }
                    }
                    if(check)
                    {
                        Launcher.ajoutMail(mail.getText());
                        Launcher.ajoutMdp(password);
                        JOptionPane.showMessageDialog(this, "Inscription re�u","Inscription",JOptionPane.INFORMATION_MESSAGE);
                        int u;
                		for(u=0;u<Launcher.getListePage().size();u++) {
                			Launcher.getListePage().get(u).setVisible(false);
                		}
                		Launcher.getListePage().get(1).setVisible(true);
                    }
                }
        });

        contentPaneCenter.add(msgMail);
        contentPaneCenter.add(mail);
        contentPaneCenter.add(msgMdp);
        contentPaneCenter.add(mdp);
        contentPaneCenter.add(msgLink);
        contentPaneCenter.add(link);
        contentPaneCenter.add(connexion);

        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());

        JButton retour = new JButton(new LienToAction("Retour",0));
        
        contentPaneBottom.add(retour,BorderLayout.SOUTH);

        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);

        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
        }
    }

