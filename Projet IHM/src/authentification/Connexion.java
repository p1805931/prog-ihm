package authentification;

import java.awt.*;
import javax.swing.*;
import accueil.LienToAction;
import application.Launcher;
import application.MenuPrincipal;

public class Connexion extends JFrame{


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Connexion() {
		
        super();
        build();
        insert();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Display the window.
        this.setVisible(false);
    } 

    private void build() {
        //set up the window.
        setTitle("Se connecter");
        setSize(360,748);

        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
    }

    private void insert() {
        //Global

        JPanel contentPane = new JPanel();
        contentPane.setLayout(new GridLayout(3,1));

        JPanel contentPaneUp = new JPanel();
        contentPaneUp.setLayout(new BorderLayout());

        JLabel title = new JLabel("Entrez vos identifiants");
        contentPaneUp.add(title);

        JPanel contentPaneCenter = new JPanel();
        contentPaneCenter.setLayout(new GridLayout(4,1));
        
        JLabel msgMail = new JLabel("Adresse mail  :");
        JTextField mail = new JTextField();
        JLabel msgMdp = new JLabel("Mot de passe  :");
        JPasswordField mdp = new JPasswordField();
        JButton connexion = new JButton("CONNEXION"); /* Connexion */
        connexion.addActionListener(event ->
        {
        	String password = new String(mdp.getPassword());
           boolean erreur = find(mail.getText(), password);
            if(erreur) {
                JOptionPane.showMessageDialog(this,
                        "Mot de passe ou email incorrect",
                        " ERREUR ",
                        JOptionPane.WARNING_MESSAGE);
            }
            if (erreur==false) {
            	
            		int i;
            		for(i=0;i<Launcher.getListePage().size();i++) {
            			Launcher.getListePage().get(i).setVisible(false);
            		}
            		((MenuPrincipal) Launcher.getListePage().get(5)).setBienvenue(mail.getText());
            		Launcher.getListePage().get(5).setVisible(true);
            }
        });

        JButton mdpforget = new JButton(new LienToAction("Mot de passe oublie ?",2)); /*oublier*/

        contentPaneCenter.add(msgMail);
        contentPaneCenter.add(mail);
        contentPaneCenter.add(msgMdp);
        contentPaneCenter.add(mdp);
        contentPaneCenter.add(connexion);
        contentPaneCenter.add(mdpforget);

        JPanel contentPaneBottom = new JPanel();
        contentPaneBottom.setLayout(new BorderLayout());

        JButton retour = new JButton(new LienToAction("Retour",0));
        contentPaneBottom.add(retour,BorderLayout.SOUTH);

        contentPane.add(contentPaneUp);
        contentPane.add(contentPaneCenter);
        contentPane.add(contentPaneBottom);

        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
    }
    
    public boolean find(String mail , String mdp) {
    	int i;
        boolean erreur = true;
        for(i=0 ; i < Launcher.getMailList().size(); i++)
        {
            if (mail.equals(Launcher.getMailList().get(i)) && mdp.equals(Launcher.getMdpList().get(i)))
            {
                Launcher.setIndice(i);
            	erreur = false;   
            }

        }
        return erreur;
    }
}
