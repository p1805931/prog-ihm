package accueil;

import java.awt.*;
import javax.swing.*;

public class EcranDeDemarrage extends JFrame{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EcranDeDemarrage() {
		
		super();
		build();
		insert();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Display the window.
        this.setVisible(true);
	}
	
	private void build() {
		//set up the window.
        setTitle("Automobile Gestion");
        setSize(360,748);
        
        Image icon = Toolkit.getDefaultToolkit().getImage("assets/logoPetit.png");
        this.setIconImage(icon);
        
        setLocationRelativeTo(null);
        setResizable(false);
	}
	
	private void insert() {
		//Global        
        JPanel contentPane = new JPanel();
        ImageIcon logo = new ImageIcon("assets/logo.png");
        JLabel image = new JLabel (logo);
        JButton demarrage = new JButton(new LienToAction("Bienvenue",0));
        contentPane.add(image);
        contentPane.add(demarrage,BorderLayout.CENTER);       
        contentPane.setOpaque(true);
        this.setContentPane(contentPane);
	}
}
