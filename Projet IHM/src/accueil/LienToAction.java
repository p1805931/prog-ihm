package accueil;

import javax.swing.*;
import java.awt.event.*;
import application.Launcher;

public class LienToAction extends AbstractAction{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int numPage;
	
	public LienToAction(String txt,int numPage) {
		super(txt);
		this.numPage = numPage;
	}
	
	public void actionPerformed(ActionEvent e) {
		int i;
		for(i=0;i<Launcher.getListePage().size();i++) {
			Launcher.getListePage().get(i).setVisible(false);
		}
		Launcher.getListePage().get(numPage).setVisible(true);
	}
}

